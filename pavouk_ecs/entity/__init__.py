from dataclasses import dataclass


@dataclass
class Entity:
    id: int
    mask: int = 0
    dead: bool = False
    tag: str = ""
    parent = None
    children = {}

    def add_mask(self, mask):
        self.mask = self.mask | mask

    def remove_mask(self, mask):
        self.mask = mask ^ self.mask

    def has_mask(self, mask):
        return mask & self.mask == mask

    def set_dead(self):
        self.dead = True

    def is_dead(self):
        return self.dead

    def add_child(self, e, name):
        self.children[name] = e

    def get_child(self, name):
        return self.children[name]

    def get_children(self):
        return list(self.children.values())
